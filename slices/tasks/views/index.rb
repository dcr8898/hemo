# frozen_string_literal: true

require "htmx"

module Tasks
  module Views
    # Renders main index view.
    class Index < Tasks::View
      expose :tasks
    end
  end
end
