# frozen_string_literal: true

module Home
  module Actions
    # Processes show action.
    class Show < Home::Action
      def handle(*, response) = response.render view
    end
  end
end
