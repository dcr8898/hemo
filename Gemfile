# frozen_string_literal: true

ruby File.read(".ruby-version").strip

source "https://rubygems.org"

gem "dry-types", "~> 1.7"
gem "hanami", github: "hanami/hanami", branch: "main"
gem "hanami-cli", github: "hanami/cli", branch: "main"
gem "hanami-controller", github: "hanami/controller", branch: "main"
gem "hanami-router", github: "hanami/router", branch: "main"
gem "hanami-utils", github: "hanami/utils", branch: "main"
gem "hanami-validations", github: "hanami/validations", branch: "main"
gem "hanami-view", github: "hanami/view", branch: "main"
gem "htmx", "~> 0.3"
gem "pg", "~> 1.5"
gem "puma", "~> 6.3"
gem "rack-attack", "~> 6.6"
gem "refinements", "~> 11.0"
gem "rom", "~> 5.3"
gem "rom-sql", "~> 3.6"
gem "sequel", "~> 5.68"

group :code_quality do
  gem "caliber", "~> 0.35"
  gem "git-lint", "~> 6.0"
  gem "reek", "~> 6.1", require: false
  gem "rubocop-sequel", "~> 0.3"
  gem "simplecov", "~> 0.22", require: false
end

group :development, :test do
  gem "dotenv", "~> 2.8"
end

group :development do
  gem "localhost", "~> 1.1"
  gem "rake", "~> 13.0"
  gem "rerun", "~> 0.14"
end

group :test do
  gem "capybara", "~> 3.39"
  gem "cuprite", "~> 0.14"
  gem "database_cleaner-sequel", "~> 2.0"
  gem "guard-rspec", "~> 4.7", require: false
  gem "launchy", "~> 2.5"
  gem "rack-test", "~> 2.1"
  gem "rom-factory", "~> 0.11"
  gem "rspec", "~> 3.12"
end

group :tools do
  gem "amazing_print", "~> 1.4"
  gem "debug", "~> 1.8"
end
